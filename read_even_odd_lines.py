# -*- coding: utf-8 -*-
"""
Created on Wed Nov 14 15:31:45 2018

@author: Balamuralikrishna_Do
"""
fname = input("Please enter the name of the file you wish to open: ")
odd_even = input("Would you like the even or odd lines? ")
odd_even = 0 if odd_even.lower() == "odd" else 1
with open(fname) as fileOne:
    for count, line in enumerate(fileOne):
        if count % 2 == odd_even:
            print(line)
