# -*- coding: utf-8 -*-
"""
Created on Wed Nov 14 16:38:00 2018

@author: Balamuralikrishna_Do
"""

def main(request):
  #Open the file back and read the contents
   f=open("sample.txt", "r")
   if f.mode == 'r':
       contents =f.read()
       print (contents)
  #or, readlines reads the individual line into a list
   fl =f.readlines()
   for x in fl:
       print (x)
   return render(request,'first_app/main.html') 